﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demp
{
    /// <summary>
    /// Repository to manage Products.
    /// </summary>
   public  class ProductRepository : IProductRepository
    {

        public Product GetProduct()
        {
            Product product = new Product(25, "Mouse", 220, "Logitech");
            return product;
        }
        public List<Product> GetAllProducts()
        {
            List<Product> products = new List<Product>();

            Product product = new Product(25, "Mouse", 220, "Logitech");
            Product product1 = new Product(26, "Mouse", 220, "Logitech");
            Product product2 = new Product(27, "Mouse", 220, "Logitech");
            Product product3 = new Product(28, "Mouse", 220, "Logitech");


            products.Add(product);
            products.Add(product1);
            products.Add(product2);
            products.Add(product3);

            return products;
        }

    }
}
