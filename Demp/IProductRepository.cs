﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demp
{
    public interface IProductRepository
    {
        Product GetProduct();
        List<Product> GetAllProducts();
    }
}
