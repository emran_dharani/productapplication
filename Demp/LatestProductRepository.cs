﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demp
{
    public class LatestProductRepository : IProductRepository
    {
        public List<Product> GetAllProducts()
        {
            List<Product> products = new List<Product>();

            Product product = new Product(29, "Optical Mouse", 220, "Logitech");
            Product product1 = new Product(30, "Optical Mouse", 220, "Logitech");
            Product product2 = new Product(31, "Optical Mouse", 220, "Logitech");
            Product product3 = new Product(32, "Optical Mouse", 220, "Logitech");


            products.Add(product);
            products.Add(product1);
            products.Add(product2);
            products.Add(product3);

            return products;
        }

        public Product GetProduct()
        {
            Product product = new Product(25, "optimal Mouse", 220, "Logitech");
            return product;
        }
    }
}
