﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Product product = new Product();
            //product.ID = 25;
            //product.Name = "Emran";
            //product.Price = 220;
            //product.Description = "high imp";

            //Interface, Collections
            //Product product = new Product()
            //{
            //    ID = 25,
            //    Name = "Emran",
            //    Price = 220,
            //    Description = "high imp"
            //};

            //   Product product = new Product(25, "Emran", 220, "Hihgh Temp");

            IProductRepository repository;


            //ProductRepository repository = new ProductRepository();

            //repository = new ProductRepository();
            repository = new LatestProductRepository();

            //Product p1 = repository.GetProduct();


            //Console.WriteLine("ID: " + p1.Name);
            //Console.WriteLine("The Product Id is: " + p1.ID);
            //Console.WriteLine("Product Price:" + p1.Price);
            //Console.WriteLine("Description of Product:" + p1.Description);



            List<Product> items = repository.GetAllProducts();

            foreach (var item in items)
            {
                Console.WriteLine(" ID: " + item.ID + " Name: " + item.Name + " Desc: " + item.Description);

                Console.WriteLine("--------------------------------------");
            }


            Console.ReadLine();
        }
    }
}
