﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demp
{
   public class Product
    {

        public Product()
        { }
        public Product(int id, string name, decimal price, string desc)
        {

            this.ID = id;
            this.Name = name;
            this.Price = price;
            this.Description = desc;


        }
        // public  int Id;
        private int _id;

        //Property

        public int ID {

            get {
                return _id;
            }

            set {
                _id = value;
                //addional code
            }
        }
        private string _name;
        public string Name { get => _name; set => _name = value; }

        public decimal Price { get; set; }

        public string Description { get; set; }
    }
}
